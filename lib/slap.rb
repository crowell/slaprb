require 'httparty'
require 'colorize'
require 'highline/import'
require 'json'

class Slap
    def getToken
        puts "[!] GitHub API Token needed to use gists".red
        _username = ask("USERNAME: ".green) {|q| q.echo = true}
        _password = ask("PASSWORD: ".green) {|q| q.echo = false}
        _headers = Hash.new
        _headers["Content-Type"] = "application/json"
        _headers["User-Agent"] = "Slap, not at all like gecko"
        _auth = {:username=>_username, :password=>_password}
        _options = {
            :headers => _headers,
            :basic_auth => _auth,
            :body => {
                "scopes" => ["gist", "repo"],
                "note" => "slap",
                "note_url" => "http://raxcity.com/slap"
            }.to_json,
        }

        _resp = HTTParty.post("https://api.github.com/authorizations", _options)

        #puts "---DEBUG---".green
        #puts _resp
        #puts "---DEBUG---".green
        
        if _resp.code == 201
            puts "[+] got and saved token".green
        else
            puts "[!] unable to get token: #{_resp["message"]}".red
            return nil
        end
        _token = _resp["token"]
        return _token
    end

    def loadToken
        _dotfile = ENV['HOME']+"/.slaprc"
        if File.file?(_dotfile)
            _token = File.open(_dotfile, "rb"){|io| io.read}
        else
            return nil
        end
        return _token
    end

    def saveToken(token)
        _dotfile = ENV['HOME']+"/.slaprc"
        File.open(_dotfile, "w"){|io| io.write(token)}
    end

    def getAuthed
        _tok = loadToken
        if not _tok.nil?
            return _tok
        end
        _tok = getToken
        if not _tok.nil?
            saveToken(_tok)
            return _tok
        end
        puts "[!] ERROR: could not get auth token!".red
        return nil
    end

    def postGist(files, description, pub, anon)
        _headers= Hash.new
        _headers["Content-Type"] = "application/json"
        _headers["User-Agent"] = "Slap, not at all like gecko"
        if anon == false
            tok = getAuthed
            if tok.nil?
                return nil
            end
            _headers["Authorization"] = "token #{tok}"
        else
            #do nothing
        end

        _options = {
            :body => {
                "description" => description,
                "public" => pub,
                "files" => files
            }.to_json,
            :headers => _headers
        }
        _resp = HTTParty.post("https://api.github.com/gists", _options)
        #puts "---DEBUG---".green
        #puts _resp
        #puts "---DEBUG---".green
        return _resp
    end
end
