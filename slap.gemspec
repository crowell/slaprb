Gem::Specification.new do |s|
    s.name        = 'slap'
    s.version     = '0.0.1'
    s.date        = '2013-11-28'
    s.summary     = "slap"
    s.executables << "slap"
    s.description = "a command line client for github's gists"
    s.authors     = ["Jeffrey Crowell"]
    s.email       = 'crowell@bu.edu'
    s.files       = ["lib/slap.rb"]
    s.homepage    = 'https://bitbucket.org/crowell/slaprb'
    s.license     = 'MIT'
    s.post_install_message = "slap installed *<:-)"
    s.add_runtime_dependency 'httparty'
    s.add_runtime_dependency 'colorize'
    s.add_runtime_dependency 'trollop'
    s.add_runtime_dependency 'highline'
    s.add_runtime_dependency 'clipboard'
end
